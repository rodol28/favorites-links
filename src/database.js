const { database } = require('./keys');
const mysql = require('mysql');

const mysqlConnection = mysql.createConnection(database);

mysqlConnection.connect((err) => {
    if(err) {
        console.log(err);
        return;
    } else {
        console.log('db is connected');
    }
});


module.exports = mysqlConnection;