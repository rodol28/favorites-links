const passport = require('passport');

var control = {};

// show signup
control.showSignup = (req, res) => {
    res.render('auth/signup');
};

// signup
control.signup = passport.authenticate('local.signup', {
    successRedirect: '/profile',
    failureRedirect: '/signup',
    failureFlash: true
});

// show signin
control.showSignin = (req, res) => {
    res.render('auth/signin');
};

// signin
control.signin = passport.authenticate('local.signin', {
    successRedirect: '/profile',
    failureRedirect: '/signin',
    failureFlash: true
});

// show profile
control.profile = (req, res) => {
    res.render('profile');
};

// close session
control.logout = (req, res) => {
    req.logOut();
    res.redirect('/signin');
};

module.exports = control;