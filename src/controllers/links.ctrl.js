const pool = require("../database");

var control = {}

// show view add link
control.showAdd = (req, res) => {
    res.render('links/add');
};

// add a new link
control.add = (req, res) => {
    const { title, url, description } = req.body;
    
    const newLink = {
        title,
        url,
        description,
        user_id: req.user.id
    };

    pool.query('INSERT INTO links SET ?', [newLink], function (error, results) {
        if (error) throw error;
        console.log('Saved');
        req.flash('success_msg', 'Link saved successfully.');
        res.redirect('/links');
      });
    
};

// show all links
control.showLinks = (req, res) => {
    pool.query('SELECT * FROM links WHERE user_id=?', [req.user.id], function (error, links) {
        if (error) throw error;
        res.render('links/list', {links});
      });
};

// remove a link
control.delete = (req, res) => {
    const { id } = req.params;
    pool.query('DELETE FROM links WHERE id=' + id, function (error, links) {
        if (error) throw error;
        req.flash('success_msg', 'Link removed successfully.');
        res.redirect('/links');
      });
};

//show view edit a link
control.showEdit = (req, res) => {
    const { id } = req.params;

    pool.query('SELECT * FROM links WHERE id=' + id, function (error, link) {
        if (error) throw error;
        res.render('links/edit', {links: link[0]});
      });
};

// update a link
control.update = (req, res) => {
    const { id } = req.params;
    const {title, url, description} = req.body;
    
    const newLink = {
        title,
        url,
        description
    };

    pool.query('UPDATE links SET ? WHERE id=' + id, [newLink], function (error, results) {
        if (error) throw error;
        console.log('Update');
        req.flash('success_msg', 'Link updated successfully.');
        res.redirect('/links');
      });
};

module.exports = control;