const express = require('express');
const path = require('path');
const morgan = require('morgan');
const exphbs = require('express-handlebars');
const flash = require('connect-flash');
const session = require('express-session');
const MySQLStore = require('express-mysql-session')(session);
require('dotenv').config();
const { database } = require('../keys');
const passport = require('passport');

module.exports = app => {

    // initializantions
    require('../lib/passport');

    // settings
    app.set('port', process.env.PORT || 3000);
    app.set('views', path.join(__dirname, '../views'));
    app.engine('.hbs', exphbs({
        defaultLayout: 'main',
        layoutsDir: path.join(app.get('views'), 'layouts'),
        extname: '.hbs',
        helpers: require('../lib/handlebars')
    }));
    app.set('view engine', '.hbs');

    // middlewares
    if (process.env.NODE_ENV === 'development') {
        app.use(morgan('dev'));
    }

    app.use('/public', express.static(path.join(__dirname, '../public')));
    app.use(express.urlencoded({
        extended: false
    }));
    app.use(express.json());

    app.use(session({
        secret: process.env.KEY,
        resave: false,
        saveUninitialized: false,
        store: new MySQLStore(database)
    }));
    app.use(flash());
    app.use(passport.initialize());
    app.use(passport.session());

    //global variables
    app.use((req, res, next) => {
        res.locals.success_msg = req.flash('success_msg');
        res.locals.message = req.flash('message');
        app.locals.user = req.user;
        
        next();
    });

    // routes
    app.use(require('../routes/index.routes'));
    app.use(require('../routes/authentication.routes'));
    app.use('/links', require('../routes/links.routes'));

    return app
};