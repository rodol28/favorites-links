const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const pool = require('../database');
const helpers = require('./helpers');

passport.use('local.signin', new LocalStrategy({
    usernameField: 'username',
    passwordField: 'password',
    passReqToCallback: true
}, (req, username, password, done) => {
    pool.query('SELECT * FROM users WHERE username=?', [username], async (error, rows) => {
        if(error) throw error;
        
        if(rows.length > 0) {
            const user = rows[0];
            const validPassword = await helpers.matchPassword(password, user.password);
            if(validPassword) {
                done(null, user, req.flash('success_msg', 'Welcome ' + user.username));
            } else {
                done(null, false, req.flash('message', 'Incorrect password'));
            }
        } else {
            return done(null, false, req.flash('message', 'The username does not exists'));
        }
    });
}));

passport.use('local.signup', new LocalStrategy({
    usernameField: 'username',
    passwordField: 'password',
    passReqToCallback: true
}, async (req, username, password, done) => {
    const fullname = req.body.fullname;
    const newUser = {
        username,
        password,
        fullname
    };

    newUser.password = await helpers.encryptPassword(password);
    pool.query('INSERT INTO users SET ?', [newUser], (error, result) => {
        if (error) throw error;
        console.log('Saved');

        newUser.id = result.insertId;
        return done(null, newUser);
    });
}));

passport.serializeUser((user, done)=> {
    done(null, user.id);
});

passport.deserializeUser((id, done) => {
    pool.query('SELECT * FROM users WHERE id=?', [id], (error, rows) => {
        if(error) throw error;
        done(null, rows[0]);
    });
});