const ctrl = require('../controllers/links.ctrl');
const router = require('express').Router();
const { isLoggedIn } = require('../lib/auth'); 

const pool = require('../database');

router.get('/', isLoggedIn, ctrl.showLinks);
router.get('/add', isLoggedIn, ctrl.showAdd);
router.post('/add', isLoggedIn, ctrl.add);
router.get('/delete/:id', isLoggedIn, ctrl.delete);
router.get('/edit/:id', isLoggedIn, ctrl.showEdit);
router.post('/edit/:id', isLoggedIn, ctrl.update);

module.exports = router;