const router = require('express').Router();
const ctrl = require('../controllers/auth.ctrl');
const { isLoggedIn, isNotLoggedIn } = require('../lib/auth');

router.get('/signup', isNotLoggedIn ,ctrl.showSignup);
router.post('/signup', ctrl.signup);

router.get('/signin', isNotLoggedIn, ctrl.showSignin);
router.post('/signin', ctrl.signin);
router.get('/logout', ctrl.logout);

router.get('/profile', isLoggedIn, ctrl.profile);

module.exports = router;